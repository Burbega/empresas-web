import React from 'react';
import './Search.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Page where companies would be shown
function Search() {
  return (
    <React.StrictMode>
      <div className="App">
        <nav className="navbar navbar-light bg-light ">
          <div className="App">
            <h2 className="text-center" id="pesquisa">
              Clique na busca para iniciar.
            </h2>
          </div>
        </nav>
      </div>
    </React.StrictMode>
  );
}

export default Search;
