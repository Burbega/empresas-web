import React from 'react';
import {
  BrowserRouter, Route, Link, Switch,
} from 'react-router-dom';

// ioasys logo
import logo from '../../assets/imgs/logo-home.png';
import './Home.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Search page
import Search from './Search';

// Home: Initial project page
// Incomplete form/api integration
function Home() {
  return (
    <>
      <BrowserRouter>
        <React.StrictMode>
          <div className="App">
            <div className="login-screen">
              <div className="container">
                <div className="row d-flex justify-content-center align-items-center">
                  <div className="Rectangle-26 align-self-center">

                    {/* Clicking the ioasys logo takes the user to the Homepage */}
                    <a href="/" className="navbar-brand center-block">
                      <img src={logo} alt="logo" />
                    </a>
                    <h2 className="welcome">BEM-VINDO AO EMPRESAS</h2>
                    <p className="lorem-ipsum">
                      Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc
                      accumsan.
                    </p>
                    <form>
                      <div className="form-group">
                        <input
                          type="email"
                          className=""
                          id="exampleInputEmail1"
                          aria-describedby="emailHelp"
                          placeholder="E-mail"
                        />
                      </div>
                      <div className="form-group">
                        <input
                          type="password"
                          className=""
                          id="exampleInputPassword1"
                          placeholder="Senha"
                        />
                      </div>

                      {/* Clicking the ENTRAR button leads the
                      user to the internal page (Unfinished) */}
                      <Link to="/search">
                        <button
                          type="submit"
                          className="btn btn-primary text-uppercase login-button"
                        >
                          Entrar
                        </button>
                      </Link>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.StrictMode>
        <Switch>
          <Route path="/search" component={Search} />
        </Switch>
      </BrowserRouter>
      ;
    </>
  );
}

export default Home;
