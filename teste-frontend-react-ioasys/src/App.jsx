import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './components/template/Home';

function App() {
  return (
    <React.StrictMode>
      {/* Initial project page */}
      <Home />
    </React.StrictMode>
  );
}

export default App;
